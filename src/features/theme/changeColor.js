import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    value: "#ff5544",
};
function getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

export const changeColor = createSlice({
    name: "color",
    initialState,
    reducers: {
        change: (state) => {
            state.value = getRandomColor();
        },
    },
});

export const { change } = changeColor.actions;

export default changeColor.reducer;
