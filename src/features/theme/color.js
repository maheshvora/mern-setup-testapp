import React from "react";
import { useDispatch } from "react-redux";
import { change } from "./changeColor";

export function Color() {
    const dispatch = useDispatch();

    return (
        <div>
            <div>
                <button onClick={() => dispatch(change())}>
                    Click here...
                </button>
            </div>
        </div>
    );
}
