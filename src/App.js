import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState, useEffect } from "react";
import { Color } from "./features/theme/color";
import { useSelector } from "react-redux";
function App() {
    const [data, setData] = useState(null);
    const color = useSelector((state) => state.color.value);
    console.log(process.env.NODE_ENV, "environment");
    console.log(process.env, "environment json");

    useEffect(() => {
        var url = "";
        if (process.env.NODE_ENV === "development") {
            url =
                "https://mv-testapp-backend.netlify.app/.netlify/functions/api";
        } else {
            url =
                "https://mv-testapp-backend.netlify.app/.netlify/functions/api";
        }
        console.log(url, "url");
        fetch(`${url}/get_name`)
            .then((res) => res.json())
            .then((data) => {
                console.log(data);
                setData(data);
            });
    }, []);
    return (
        <div className="App">
            <div className="App">
                <header className="App-header">
                    <h1>Welcome to the testApp</h1>
                    <p>
                        {!data
                            ? "Loading..."
                            : data.map(({ firstName, lastName }) => (
                                  <h3
                                      style={{ color: `${color}` }}
                                  >{`Hi ${firstName} ${lastName}!`}</h3>
                              ))}
                    </p>
                </header>
                <Color />
            </div>
        </div>
    );
}

export default App;
