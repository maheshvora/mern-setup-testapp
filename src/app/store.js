import { configureStore } from "@reduxjs/toolkit";
import changeColor from "../features/theme/changeColor";
export const store = configureStore({
    reducer: { color: changeColor },
});
